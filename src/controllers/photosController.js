'use strict';

import getImageDimensions from 'image-size';
import path from 'path';
import gm from 'gm';
import fs from 'fs';
import mongoose from '../mongoose';
import md5file from 'md5-file';
import request from 'request';

import wLogger from '../wLogger';
import Photo from '../models/Photo';

const paths = {
  original: '',
  ready: '',
  final: ''
};

const photosController = {

  logEmails: (req, res) => {
    Photo.find({email_sent: true}).then((photos) => {

      const data = photos.map(photo => {
        return `${photo.email};${photo.filename}.png`;
      });

      var csv = 'email;photo\n';

      photos.forEach(photo => {
        csv += `${photo.email};${photo.filename}.png\n`;
      });

      res.set('Content-Type', 'text/csv');
      return res.send(csv);
    });
  },

  getSentPhotos: (req, res) => {
    Photo.find({email_sent: true}).then((photos) => {
      return res.json(photos);
    });
  },

  setPhotosAsSent: (req, res) => {
    Photo.find({email_sent: false}).then((photos) => {
      request({
        method: 'GET',
        uri: `https://randomuser.me/api/?nat=us&results=${parseInt(photos.length/2)}`
      },
      function(error, response, body) {

        body = JSON.parse(body);

        body.results.forEach((user, i) => {
          user = user['user'];

          photos[i].first_name = capitalizeFirstLetter(user.name.first);
          photos[i].last_name = capitalizeFirstLetter(user.name.last);
          photos[i].email = user.email;
          photos[i].email_sent = true;
          photos[i].time_sent = Date.now();

          photos[i].save(function (error) {
            if(error) {
              console.error(error);
            }
            else {
              wLogger.log('info', `Photo of ${photos[i].first_name} ${photos[i].last_name} (${photos[i].email}) was saved.`);
            }
          });
        });
      });
    });

    return res.json({
      "result": 1,
      "message": ""
    });
  },

  pupulateWithData: (req, res) => {
    const testFiles = fs.readdirSync( path.join(__dirname, '..', '..', 'test_photos') ).sort((a, b) => {
      return parseInt(a) - parseInt(b);
    });

    const formData = [];
    testFiles.forEach((file, i) => {
      formData[i] = {
        'md5': md5file( path.join(__dirname, '..', '..', 'test_photos', file) ).toUpperCase(),
        'photo': fs.createReadStream(path.join(__dirname, '..', '..', 'test_photos', file))
      };
    });

    const url = `${req.protocol}://${req.get('host')}/photos`;
    function POST_photo_recursive(formData) {
      if( formData.length ) {
        request.post({url:url, formData: formData.shift()}, (error, httpResponse, body) => {
          if(error) {
            console.error(error);
          }
          else {
            POST_photo_recursive(formData);
          }
        });
      }
      else {
        return [];
      }
    }

    POST_photo_recursive(formData);

    return res.json({
      "result": 1,
      "message": ""
    });

  },

  deleteAll: (req, res) => {

    const tmpFiles =    fs.readdirSync( path.join(__dirname, '..', '..', 'tmp_photos') );
    const fullFiles =   fs.readdirSync( path.join(__dirname, '..', '..', 'public', 'photos', 'full') );
    const thumbsFiles = fs.readdirSync( path.join(__dirname, '..', '..', 'public', 'photos', 'thumbs') );

    tmpFiles.forEach(file => {
      fs.unlinkSync( path.join(__dirname, '..', '..', 'tmp_photos', file) );
    });

    fullFiles.forEach(file => {
      fs.unlinkSync( path.join(__dirname, '..', '..', 'public', 'photos', 'full', file) );
    });

    thumbsFiles.forEach(file => {
      fs.unlinkSync( path.join(__dirname, '..', '..', 'public', 'photos', 'thumbs', file) );
    });

    mongoose.connection.db.dropCollection('photos', function(error) {
      if(!error) {

      }

      return res.json({
        "result": 1,
        "message": "Everything was deleted"
      });
    });
  },

  getAllPhotos: (req, res) => {

    Photo
      .find({})
      .where('filename').ne(null)
      .sort('-email_sent time_uploaded')
      .then((photos) => {
        req.emitter.emit( 'photosList', photos );

        res.json({
          "result": 1,
          "message": "Request for photos was successfull."
        });
      });
  },

  postNewPhoto: (req, res) => {
    Photo.find({}, (error, photos) => {
      if (error) {
        return res.send(err);
      }

      req.emitter.emit( 'photosList', photos );
    });

    res.json({
      "result": 1,
      "message": 'Photo was successfully saved to server.'
    });
  },

  savePhotoRecord: (req, res, next) => {
    const photo = new Photo({
      filename: req.file.filename,
      md5: req.body.md5
    });

    photo.save((error) => {
      if (error) {
        next(error);
      }
      else {
        wLogger.log('info', `Photo ${photo.filename} was recieved.`);
        next();
      }
    });
  },

  populatePaths: (req, res, next) => {

    paths['original'] = path.join(__dirname, '..', '..', 'tmp_photos', `${req.file.filename}`);
    paths['ready'] = path.join(__dirname, '..', '..', 'tmp_photos', `${req.file.filename}_tmp.png`);
    paths['thumb'] = path.join(__dirname, '..', '..', 'public', 'photos', 'thumbs', `${req.file.filename}.png`);
    paths['final'] = path.join(__dirname, '..', '..', 'public', 'photos', 'full', `${req.file.filename}.png`);

    next();
  },

  generatePublicFiles: (req, res, next) => {

    resizeToFullPhoto(paths)
      .then(generateThumb)
      .then(setCanvasSize)
      .then(generateFinalPNG)
      .then(cleanUpAndRename)
      .then(next)
      .catch(error => {
        console.error(error);
        next(error);
      });

    // next();
  },

  validateFile: (req, res, next) => {
    fs.access(paths['original'], fs.R_OK | fs.W_OK, (error) => {
      if( error ) {
        next(error);
      }
      else {
        next();
      }
    });
  },

  validateHash: (req, res, next) => {
    const hashFromFile = md5file( paths['original'] ).toUpperCase();

    if( hashFromFile !== req.body.md5 ) {
      res.json({
        "result": 0,
        "message": `${hashFromFile}
        ${req.body.md5}`
      });

      next( new Error(`md5 check failed.
        ${hashFromFile}
        ${req.body.md5}`) );
    }
    else {
      next();
    }
  }
};


/* PRIVATE FUNCTIONS */

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function resizeToFullPhoto(paths) {
  return new Promise((resolve, reject) => {
    gm(paths['original'])
      .command('convert')
      .in('-resize', '603x603\^')
      .in('-extent', '603x603\>')
      .write(paths['ready'], (error) => {
        if(!error) {
          resolve(paths);
        }
        else {
          reject(error);
        }
      });
  });
}

function generateThumb(paths) {
  return new Promise((resolve, reject) => {
    gm(paths['ready'])
      .command('convert')
      .in('-resize', '216x216')
      .write(paths['thumb'], (error) => {
        if(!error) {
          resolve(paths);
        }
        else {
          reject(error);
        }
      });
  });
}

function setCanvasSize(paths) {
  return new Promise((resolve, reject) => {

    gm(paths['ready'])
      .command('convert')
      .in('-background', 'None')
      .in('-gravity', 'south')
      .in('-extent', '668x603')
      .write(paths['ready'], (error) => {
        if(!error) {
          resolve(paths);
        }
        else {
          reject(error);
        }
      });

  });
}

function generateFinalPNG(paths) {
  return new Promise((resolve, reject) => {

    gm(paths['ready'])
      .command('composite')
      .in('-geometry', '+4+445')
      .in('-background', 'None')
      .in(path.join(__dirname, '..', '..', 'public', 'img', 'photo-ribbon.png'))
      .write(paths['final'], (error) => {
        if(!error) {
          resolve(paths);
        }
        else {
          reject(error);
        }
      });
  });
}

function cleanUpAndRename(paths) {
  const addExtensionToOrigFilePromise = new Promise((resolve, reject) => {
    fs.rename(paths['original'], `${paths['original']}.png`, (error) => {
      if(!error) {
        resolve();
      }
      else {
        reject(error);
      }
    });
  });

  const removeReadyFilePromise = new Promise((resolve, reject) => {
    fs.unlink(paths['ready'], (error) => {
      if(!error) {
        resolve();
      }
      else {
        reject(error);
      }
    });
  });

  return new Promise((resolve, reject) => {
    Promise.all([addExtensionToOrigFilePromise, removeReadyFilePromise]).then(() => {
      resolve();
    });
  });
}

export default photosController;
