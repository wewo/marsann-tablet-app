import Photo from '../models/Photo';
import path from 'path';
import config from '../config/config';
import wLogger from '../wLogger';

let mainController = {

  drawing: (req, res, next) => {
    res.render('drawing');
  },

  getIndex: (req, res, next) => {
    res.render('index');
  },
  formSubmit: (req, res, next) => {

    sendPhotoToEmail(req.body)
      .then(findPhoto)
      .then(savePhoto)
      .then(() => {
        res.json({
          "result": 1,
          "message": "Photo was successfully sent to email."
        });
      })
      .catch((error) => {
        res.json({
          "result": 0,
          "message": JSON.stringify(error)
        });
      });
  }
}

function sendPhotoToEmail(reqBody) {
  return new Promise((resolve, reject) => {
    const email = require("emailjs/email");
    const server = email.server.connect({
      user:    config.SMTP.user,
      password:config.SMTP.password,
      host:    config.SMTP.host,
      ssl:     true
    });

    server.send({
       from:    "Lenovo Online <info@lenovoonline.sk>",
       to:      `${reqBody.first_name} ${reqBody.last_name} <${reqBody.email}>`,
       subject: "Fotka z Business Golf Tour 2016",
       attachment:
       [
          {
            data: "<html>Držíme Vám palce v turnajoch!<br>Na pamiatku zasielame fotografiu z úplne nového smartfónu Lenovo VIBE X3.<br><br>Tím MARSANN IT</html>",
            alternative: true
          },
          {
            path: path.join(__dirname, '..', '..', 'public', 'photos', 'full', `${reqBody.filename}.png`),
            type: "image/png",
            name: "marsann_selfie.png"
          }
       ]
    },
    function(error, message) {
      if(error) {
        wLogger.log('error' ,`Email to ${reqBody.email} not sent: ${error}`);
        reject(error);
      }
      else {
        wLogger.log('info' ,`Email to ${reqBody.email} should be sent - no errors.`);
        resolve(reqBody);
      }
    });
  });
}

function findPhoto(reqBody) {

  return new Promise((resolve, reject) => {
    Photo.findById(reqBody._id, function (error, photo) {
      if(error) {
        reject(error);
      }
      else {
        resolve({
          'reqBody': reqBody,
          'photo': photo
        });
      }
    });
  });
}

function savePhoto(data) {
  return new Promise((resolve, reject) => {
    var photo = data.photo;

    photo.email = data.reqBody.email;
    photo.first_name = data.reqBody.first_name;
    photo.last_name = data.reqBody.last_name;
    photo.email_sent = true;
    photo.time_sent = Date.now();

    photo.save(function (error) {
      if(error) {
        reject(error);
      }
      else {
        wLogger.log('info', `Photo of ${photo.first_name} ${photo.last_name} (${photo.email}) was saved.`);
        resolve();
      }
    });
  });
}

export default mainController;
