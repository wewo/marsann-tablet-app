import winston from 'winston';

const wLogger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)(),
    new (winston.transports.File)({ filename: 'log.log' })
  ]
});

export default wLogger;
