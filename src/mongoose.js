import mongoose from 'mongoose';
import wLogger from './wLogger';

mongoose.connect('mongodb://localhost:27017/photoDB'); // Connects to your MongoDB.  Make sure mongod is running!
mongoose.connection.on('error', () => {
    console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
    process.exit(1);
});

export default mongoose;
