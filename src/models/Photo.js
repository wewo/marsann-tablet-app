import mongoose from 'mongoose';

const photoSchema = new mongoose.Schema({
  filename: String,
  md5: String,
  first_name: {type: String, default: ''},
  last_name: {type: String, default: ''},
  email: {type: String, default: ''},
  email_sent: {type: Boolean, default: false},
  time_uploaded: {type: Date, default: Date.now},
  time_sent: {type: Date, default: null}
});

export default mongoose.model('Photo', photoSchema);
