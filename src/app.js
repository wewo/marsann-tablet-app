/**
 * Import dependencies
 */
import express from 'express';
import logger from 'morgan'; // Logs each server request to the console
import bodyParser from 'body-parser'; // Takes information from POST requests and puts it into an object
import methodOverride from 'method-override'; // Allows for PUT and DELETE methods to be used in browsers where they are not supported
import mongoose from './mongoose'; // Wrapper for interacting with MongoDB
import path from 'path'; // File path utilities to make sure we're using the right type of slash (/ vs \)
import { EventEmitter } from 'events';

import http from 'http';
import socketIO from 'socket.io';

import wLogger from './wLogger';
import mainRouter from './routes/main';
import photosRouter from './routes/photos';
import photosController from './controllers/photosController';

/**
 * Configure database
 */


/**
 * Configure app
 */
const photosEmitter = new EventEmitter();
const app = express();
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, '..', 'views'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, '..', 'public')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(methodOverride());

app.use((req, res, next) => {
  req.emitter = photosEmitter;
  next();
});

app.use('/', mainRouter);
app.use('/photos', photosRouter);

/**
 * Start app
 */
var server = app.listen(app.get('port'), function() {
    wLogger.log('info', `App listening on port ${app.get('port')}!`);
});

// const server = http.Server(app);
const io = socketIO(server);

photosEmitter.setMaxListeners(0);

io.on('connection', (socket) => {

  wLogger.log('info', 'User connected.');

  const emitPhotosList = (data) => {
    wLogger.log('info', 'Emmiting photosList event.');
    socket.emit('photosList', data);
  };

  http.get({
    port: 3000,
    path: '/photos/',
    method: 'GET'
  }, (res) => {}).on('error', (e) => {
    wLogger.log('error' ,`Got error: ${e.message}`);
  });

  photosEmitter.on('photosList', emitPhotosList);

  socket.on('disconnect', () => {
    wLogger.log('info', 'User disconnected.');
    photosEmitter.removeListener('photosList', emitPhotosList);
  });

});
