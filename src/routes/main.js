import multer from 'multer';
import express from 'express';
const router = express.Router();

import mainController from '../controllers/mainController';

router.get(
  '/index.html',
  mainController.getIndex
);



router.get(
  '/',
  mainController.getIndex
);

router.get(
  '/drawing',
  mainController.drawing
);

router.get(
  '/drawing.html',
  mainController.drawing
);

router.post(
  '/form',
  mainController.formSubmit
);

export default router;
