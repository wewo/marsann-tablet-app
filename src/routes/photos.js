import multer from 'multer';
import express from 'express';
const router = express.Router();

import photosController from '../controllers/photosController';

router.get('/', photosController.getAllPhotos);
// router.get('/delete', photosController.deleteAll);
// router.get('/populate', photosController.pupulateWithData);
// router.get('/set', photosController.setPhotosAsSent);
router.get('/sent', photosController.getSentPhotos);
router.get('/log-emails.csv', photosController.logEmails);

router.post(
  '/',
  multer({dest:'tmp_photos/'}).single('photo'),
  photosController.populatePaths,
  photosController.validateFile,
  photosController.validateHash,
  photosController.generatePublicFiles,
  photosController.savePhotoRecord,
  photosController.postNewPhoto
);

export default router;
