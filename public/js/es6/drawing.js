$(() => {
  $.get('/photos/sent').done((winnerCandidates) => {

    var hasWinner = false;
    var winner;
    var winnerIndex;

    const startDrawing = () => {
      const drawingInterval = setInterval(() => {
        if(!hasWinner) {
          winnerIndex = getRandomNumber(winnerCandidates.length);
          winner = winnerCandidates[winnerIndex];
          if(winner) {
            $('#winner-full-name').text(`${winner.first_name} ${winner.last_name}`);
            $('#winner-email').text(`${winner.email}`);
          }
        }
        else {
          clearInterval(drawingInterval);
        }

      }, 50);
    }

    $('#toggle-drawing').on('click', function() {
      if(!hasWinner) {
        if(winner) {
          $('#winner-photo').attr('src', `photos/full/${winner.filename}.png`);
          $(this).val('Spustiť losovanie');
          hasWinner = true;
          winnerCandidates.splice(winnerIndex, 1);
        }
      }
      else {
        $('#winner-photo').attr('src', 'img/unknown-winner.png');
        hasWinner = false;
        $(this).val('Zastaviť losovanie');
        startDrawing();
      }

    });

    startDrawing();
  });



});

function getRandomNumber(max) {
  return Math.floor(Math.random() * max);
}
