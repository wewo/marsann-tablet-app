$(function() {

  var owl = $(".owl-carousel").owlCarousel({
    loop:false,
    lazyLoad:true,
    nav:false,
    items: 1,
    navContainer: '#pagination'
  });

  if(window.location.hash == '#storage') {

    let $storageTable = $('<table id="storage-table"></table>');

    $.each(localStorage, (id, email) => {
      let $tr = $('<tr></tr>');
      $tr.append(`<td>${id}.jpg</td>`);
      $tr.append(`<td>${email}.jpg</td>`);
      $storageTable.append($tr);
    });

    $('body').append($storageTable);
  }

  const socket = io();

  socket.on('photosList', (data) => {
    data = divideData(data.reverse(), 9);
    updatePhotosGrid(data);

    owl.trigger('destroy.owl.carousel');
    owl = $(".owl-carousel").owlCarousel({
      loop: data.length > 1,
      lazyLoad: true,
      nav: false,
      items: 1,
      navContainer: '#pagination'
    });

    if( data.length > 1 ) {
      $('#pagination').show();
      $('#newer').show().on('click', () => {
        owl.trigger('prev.owl.carousel');
      });
      $('#older').show().on('click', () => {
        owl.trigger('next.owl.carousel');
      });
    }

    handleGallery();
  });
});

function handlePhotoTileClick() {
  return new Promise((resolve, reject) => {
    $('#gallery .photo-tile').on('click', function() {
      const id = $(this).find('img').data('id');
      const filename = $(this).find('img').data('filename');
      const emailSent = $(this).find('img').data('email_sent');

      if(!id || !filename) {
        return;
      }

      resolve({
        "_id": id,
        "filename": filename,
        "email_sent": emailSent || false,
        "first_name": $(this).find('img').data('first_name'),
        "last_name": $(this).find('img').data('last_name'),
        "email": $(this).find('img').data('email')
      });
    });
  });
}

function showDetail(photoData) {
  return new Promise((resolve, reject) => {
    $('#detail').html(createDetail(photoData)).fadeIn(200, function() {
      resolve( this );
    });
  });
}

function addEventHandlers(detail) {
  return new Promise((resolve, reject) => {
    $(detail).find('#close-detail-btn').on('click', closeDetail);

    $('#send-mail-form').on('submit', function(event) {
      event.preventDefault();
      resolve( this );
    });
  });
}

function handleFormSubmit( mailForm ) {
  const formData = {
    "_id": $( mailForm ).find('input[name="_id"]').val(),
    "filename": $( mailForm ).find('input[name="filename"]').val(),
    "first_name": $( mailForm ).find('input[name="first_name"]').val(),
    "last_name": $( mailForm ).find('input[name="last_name"]').val(),
    "email": $( mailForm ).find('input[name="email"]').val(),
    "agree_terms": $( mailForm ).find('input[name="agree_terms"]').is(':checked')
  };

  storeDataToLocalStorage(formData);

  if( validateForm(formData) ) {
    $.ajax({
      method: 'POST',
      url: '/form',
      data: formData
    }).done(function(result) {
      $.get('/photos');
      closeDetail();
    });
  }
  else {
    addEventHandlers('#detail').then(handleFormSubmit);
  }
}

function handleGallery() {
  handlePhotoTileClick()
    .then(showDetail)
    .then(addEventHandlers)
    .then(handleFormSubmit);
}

function closeDetail() {
  handleGallery();
  $('#detail').fadeOut(200).html('');
}

function validateForm(formData) {
  if( !formData._id || !formData.first_name || !formData.last_name || !formData.email || !formData.agree_terms ) {
    return false;
  }

  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if( !re.test(formData.email) ) {
    return false;
  }

  return true;
}

function divideData(data, photosPerPage) {
  var finalData = []
  if( !photosPerPage )
    photosPerPage = 3;

  while (data.length > 0)
    finalData.push(data.splice(0, photosPerPage));

  return finalData;
}

function updatePhotosGrid(data) {
  var html = '';
  data.forEach(slideData => {
    html += '<div class="slide">';
      slideData.forEach(photoData => {
        html += createGalleryItem(photoData)
      });
    html += '</div>';
  });

  $('#gallery .owl-carousel').html(html);
}

function createGalleryItem(photoData) {
  const className = (photoData.email_sent) ? 'photo-tile sent' : 'photo-tile';
  return `<button class="${className}">
    <img
      alt=""
      width="226"
      height="226"
      src="photos/thumbs/${photoData.filename}.png"
      data-id="${photoData._id}"
      data-email="${photoData.email}"
      data-filename="${photoData.filename}"
      data-email_sent="${photoData.email_sent}"
      data-first_name="${photoData.first_name}"
      data-last_name="${photoData.last_name}" />
  </button>`;
}

function createDetail(photoData) {
  if(!photoData._id || !photoData.filename) {
    console.error('createDetail: Invalid photo data.');
    return '';
  }

  const form = `<form id="send-mail-form" action="">
    <input type="hidden" name="filename" value="${photoData.filename}">
    <input type="hidden" name="_id" value="${photoData._id}">
    <div class="form-group">
      <input type="text" name="first_name" placeholder="meno" class="form-control">
    </div>
    <div class="form-group">
      <input type="text" name="last_name" placeholder="priezvisko" class="form-control">
    </div>
    <div class="form-group">
      <input type="email" name="email" placeholder="e-mailová adresa" class="form-control">
    </div>
    <div class="checkbox">
      <label>
        <input type="checkbox" name="agree_terms">
        <div class="checkbox-placeholder"></div>
        <p>Súhlasím, aby  MARSANN IT, s.r.o. ("prevádzkovateľ") spracoval údaje uvedené vyššie vo formulári a moju fotografiu (ďalej spolu aj ako "údaje") na marketingové a reklamné účely prevádzkovateľa. Prevádzkovateľ zverejňuje údaje na internete na sociálnych sieťach prevádzkovateľa aj  v propagačných materiáloch. Tento súhlas je udelený na dobu neurčitú  a platí až do odvolania.  Poskytnuté údaje sú pravdivé, presné a úplne. V súlade s ustanovením § 12 zákona č. 40/1964 Zb. Občiansky zákonník v znení neskorších predpisov  súhlasím s vyhotovením a použitím svojej podobizne na marketingové a  reklamné účely prevádzkovateľa  na sociálnych sieťach na internete, a to bezodplatne. Osoba, ktorej údaje sú predmetom spracúvania, má práva podľa § 28 zákona č. 122/2013 Z. z. o ochrane osobných údajov v znení neskorších predpisov.</p>
      </label>
      <div class="clearfix"></div>
    </div>
    <input type="submit" value="odoslať fotku">
  </form>`;

  const alreadySent = `<div id="already-sent">
    <h3>Fotka už bola odoslaná:</h3>
    <p>${photoData.first_name} ${photoData.last_name}</p>
    <p>${photoData.email}</p>
  </div>`;

  return `<button id="close-detail-btn"></button>
    <img id="big-photo" src="photos/full/${photoData.filename}.png" alt="Fandím s LenovoOnline.sk" width="668" height="603">
    ${ photoData.email_sent ? alreadySent : form}`;
}

function getRecordsFromLocalStorage() {

    var archive = [],
        keys = Object.keys(localStorage),
        key;

    for (var i = 0; key = keys[i]; i++) {
        archive.push( key + '=' + localStorage.getItem(key));
    }

    return archive;
}

function storeDataToLocalStorage(formData) {
  localStorage.setItem(formData.filename, formData.email);
}
